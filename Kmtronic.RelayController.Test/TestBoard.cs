﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kmtronic.RelayController.Test
{
    [TestClass]
    public class TestBoard
    {
        private const string _comPort = "COM4";

        [TestMethod]
        public void CanConnect()
        {
            var controller = new FourRelayController();
            controller.Connect(_comPort);

            Assert.IsTrue(controller.IsConnected);
        }

        [TestMethod]
        public void CanTurnRelay1On()
        {
            var controller = new FourRelayController();
            controller.Connect(_comPort);

            controller.SendCommand(FourRelayController.RelayEnum.Relay1, RelayController.RelayStateEnum.On);
        }

        [TestMethod]
        public void CanTurnRelay1Off()
        {
            var controller = new FourRelayController();
            controller.Connect(_comPort);

            controller.SendCommand(FourRelayController.RelayEnum.Relay1, RelayController.RelayStateEnum.Off);
        }

        [TestMethod]
        public void CanTurnRelay2On()
        {
            var controller = new FourRelayController();
            controller.Connect(_comPort);

            controller.SendCommand(FourRelayController.RelayEnum.Relay2, RelayController.RelayStateEnum.On);
        }

        [TestMethod]
        public void CanTurnRelay2Off()
        {
            var controller = new FourRelayController();
            controller.Connect(_comPort);

            controller.SendCommand(FourRelayController.RelayEnum.Relay2, RelayController.RelayStateEnum.Off);
        }

        [TestMethod]
        public void CanTurnRelay3On()
        {
            var controller = new FourRelayController();
            controller.Connect(_comPort);

            controller.SendCommand(FourRelayController.RelayEnum.Relay3, RelayController.RelayStateEnum.On);
        }

        [TestMethod]
        public void CanTurnRelay3Off()
        {
            var controller = new FourRelayController();
            controller.Connect(_comPort);

            controller.SendCommand(FourRelayController.RelayEnum.Relay3, RelayController.RelayStateEnum.Off);
        }

        [TestMethod]
        public void CanTurnRelay4On()
        {
            var controller = new FourRelayController();
            controller.Connect(_comPort);

            controller.SendCommand(FourRelayController.RelayEnum.Relay4, RelayController.RelayStateEnum.On);
        }

        [TestMethod]
        public void CanTurnRelay4Off()
        {
            var controller = new FourRelayController();
            controller.Connect(_comPort);

            controller.SendCommand(FourRelayController.RelayEnum.Relay4, RelayController.RelayStateEnum.Off);
        }
    }
}
