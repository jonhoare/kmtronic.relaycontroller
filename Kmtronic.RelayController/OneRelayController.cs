﻿namespace Kmtronic.RelayController
{
    public class OneRelayController : RelayController
    {
        public enum RelayEnum
        {
            Relay1 = 0x01
        }

        public void SendCommand(RelayEnum relay, RelayStateEnum relayState)
        {
            SendCommand((byte)relay, (byte)relayState);
        }
    }
}
