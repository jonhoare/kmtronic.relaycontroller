﻿using System;
using System.IO.Ports;

namespace Kmtronic.RelayController
{
    public abstract class RelayController : IDisposable
    {
        private readonly SerialPort _serialPort;
        public bool IsConnected { get; set; }

        public enum RelayStateEnum
        {
            Off = 0x00,
            On = 0x01
        }

        protected RelayController()
        {
            _serialPort = new SerialPort();
        }

        public void Connect(string comPort)
        {
            try
            {
                if (_serialPort.IsOpen)
                    _serialPort.Close();

                _serialPort.PortName = comPort;
                _serialPort.Open();

                IsConnected = _serialPort.IsOpen;
            }
            catch (Exception exception)
            {
                IsConnected = false;
            }
        }

        public void Disconnect()
        {
            if (_serialPort.IsOpen)
                _serialPort.Close();
        }
        public void SendCommand(byte relay, RelayStateEnum relayState)
        {
            SendCommand(relay, (byte)relayState);
        }
        public void SendCommand(byte relay, byte relayState)
        {
            if (_serialPort.IsOpen)
                _serialPort.Write(new byte[] { 0xFF, relay, relayState }, 0, 3);
        }

        public void Dispose()
        {
            Disconnect();
            _serialPort.Dispose();
        }
    }
}