﻿namespace Kmtronic.RelayController
{
    public class FourRelayController : RelayController
    {
        public enum RelayEnum
        {
            Relay1 = 0x01,
            Relay2 = 0x02,
            Relay3 = 0x03,
            Relay4 = 0x04
        }

        public void SendCommand(RelayEnum relay, RelayStateEnum relayState)
        {
            SendCommand((byte)relay, (byte)relayState);
        }
    }
}
