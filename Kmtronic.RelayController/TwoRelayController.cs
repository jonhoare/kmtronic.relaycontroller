﻿namespace Kmtronic.RelayController
{
    public class TwoRelayController : RelayController
    {
        public enum RelayEnum
        {
            Relay1 = 0x01,
            Relay2 = 0x02,
        }

        public void SendCommand(RelayEnum relay, RelayStateEnum relayState)
        {
            SendCommand((byte)relay, (byte)relayState);
        }
    }
}
