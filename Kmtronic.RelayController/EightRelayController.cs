﻿namespace Kmtronic.RelayController
{
    public class EightRelayController : RelayController
    {
        public enum RelayEnum
        {
            Relay1 = 0x01,
            Relay2 = 0x02,
            Relay3 = 0x03,
            Relay4 = 0x04,
            Relay5 = 0x05,
            Relay6 = 0x06,
            Relay7 = 0x07,
            Relay8 = 0x08
        }

        public void SendCommand(RelayEnum relay, RelayStateEnum relayState)
        {
            SendCommand((byte)relay, (byte)relayState);
        }
    }
}
