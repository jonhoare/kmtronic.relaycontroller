# Kmtronic Relay Controller C# Wrapper #

### What is this repository for? ###

I wrote this wrapper class for my four channel Kmtronic Relay USB Board that I recently purchased and decided to make it available and Open Source.
Feel free to clone the repository, make amendments and send me any Pull Requests for changes.

* Version 1.0.0.0
* Available as an NuGet Package https://www.nuget.org/packages/Kmtronic.RelayController/
* Install from NuGet using PM > Install-Package Kmtronic.RelayController

### Using the class ###

Example:

```
#!c#
//Instantiate the class (OneRelayController, TwoRelayController, FourRelayController, EightRelayController)
var relayController = new FourRelayController();

//Connect to the USB Board passing in the COM Port that is connected.
relayController.Connect("COM4");

//Get the state of the relay controller board (Not required but just to show available properties)
var isConnected = relayController.IsConnected;

//Send a command to the board, passing in the Enum of the RelayChannel (From Intellisense) and the Enum of the state to set (From Intellisense) in this case Relay1 On 
relayController.SendCommand(FourRelayController.RelayEnum.Relay1, RelayController.RelayStateEnum.On);

//Send a command to the board, passing in the Enum of the RelayChannel (From Intellisense) and the Enum of the state to set (From Intellisense) in this case Relay1 Off
relayController.SendCommand(FourRelayController.RelayEnum.Relay1, RelayController.RelayStateEnum.Off);
```